#ifndef LIST_H
#define LIST_H

typedef struct node
{
    void *data;
    struct node *previous, *next;
} node;

typedef struct
{
    node *front;
    node *rear;
    int size;
} list;

void list_init(list *);
int list_size(list *);
void list_insert_front(list *, void *);
void list_insert(list *, int, void *);
void list_insert_rear(list *, void *);
void *list_get_front(list *);
void *list_get_rear(list *);
void list_pop_front(list *);
void list_pop_rear(list *);
void list_erase(list *, int);
void list_free(list *);

#endif
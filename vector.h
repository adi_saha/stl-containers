#ifndef VECTOR_H
#define VECTOR_H

typedef struct
{
    void **items;
    int capacity;
    int total;
} vector;

void vector_init(vector *, int);
int vector_size(vector *);
void vector_insert(vector *, int, void *);
void vector_set(vector *, int, void *);
void *vector_get(vector *, int);
void vector_push_back(vector *, void *);
void vector_pop_back(vector *, int);
void vector_clear(vector *);
void vector_free(vector *);

int vector_string_length(vector *);
void vector_stringify(vector *, char *, int);
int vector_string_length_from_char(vector *);
void vector_stringify_from_char(vector *, char *, int);

#endif
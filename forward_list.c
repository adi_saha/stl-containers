#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "forward_list.h"

void forward_list_init(forward_list *f)
{
    f->front = NULL;
    f->size = 0;
}

int forward_list_size(forward_list *f)
{
    return f->size;
}

static node *forward_list_add_node(void *data)
{
    node *newNode = (node *)malloc(sizeof(node));
    if (newNode)
    {
        newNode->data = data;
        newNode->next = NULL;
    }
    return newNode;
}

void forward_list_insert_front(forward_list *f, void *data)
{
    node *newNode = forward_list_add_node(data);

    if (f->front == NULL)
        f->front = newNode;
    else
    {
        newNode->next = f->front;
        f->front = newNode;
    }
    f->size++;
}

void forward_list_insert(forward_list *f, int index, void *data)
{
    if (index < 0 || index >= forward_list_size(f))
        return;

    node *newNode = forward_list_add_node(data);
    node *prevNode = f->front;
    for (int idx = 0; idx < forward_list_size(f); idx++, prevNode = prevNode->next)
        if (idx == index - 1)
            break;

    newNode->next = prevNode->next;
    prevNode->next = newNode;
    f->size++;
}

void forward_list_insert_after(forward_list *f, int index, void *data)
{
    forward_list_insert(f, index + 1, data);
}

void *forward_list_get_front(forward_list *f)
{
    if (forward_list_size(f))
        return f->front->data;
    return NULL;
}

void forward_list_pop_front(forward_list *f)
{
    if (forward_list_size(f))
    {
        node *temp = f->front;
        f->front = f->front->next;
        free(temp);
        f->size--;
    }
}

void forward_list_free(forward_list *f)
{
    while (forward_list_size(f))
        forward_list_pop_front(f);
}

#if 0
int main(void)
{
    forward_list f;
    forward_list_init(&f);

    forward_list_insert_front(&f, "string!");
    forward_list_insert_front(&f, "test");
    forward_list_insert_front(&f, "a");
    forward_list_insert_front(&f, "is");
    forward_list_insert_front(&f, "This");

    forward_list_insert(&f, 1, "insert-1");
    forward_list_insert_after(&f, 3, "insert-after-3");

    while (forward_list_size(&f))
    {
        printf("%s ", (char *)forward_list_get_front(&f));
        forward_list_pop_front(&f);
    }
    printf("\nSize: %d\n", forward_list_size(&f));

    forward_list_free(&f);

    return 0;
}
#endif
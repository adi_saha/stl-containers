#ifndef QUEUE_H
#define QUEUE_H

#include "list.h"

typedef struct
{
    list queue_list;
} queue;

void queue_init(queue *);
int queue_size(queue *);
void queue_push(queue *, void *);
void queue_pop(queue *);
void *queue_front(queue *);
void *queue_back(queue *);
void queue_free(queue *);

#endif
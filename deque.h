#ifndef DEQUE_H
#define DEQUE_H

typedef struct
{
    void **items;
    int capacity;
    int size;
    int startIndex;
    int headBuffer;
    int tailBuffer;
} deque;

void deque_init(deque *);
int deque_size(deque *);
void deque_push_back(deque *, void *);
void deque_pop_back(deque *);
void *deque_get_back(deque *);
void deque_push_front(deque *, void *);
void deque_pop_front(deque *);
void *deque_get_front(deque *);
void deque_free(deque *d);

#endif
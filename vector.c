#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"

void vector_init(vector *v, int capacity)
{
    v->capacity = capacity;
    v->total = 0;
    v->items = malloc(sizeof(void *) * v->capacity);
}

int vector_size(vector *v)
{
    return v->total;
}

static void vector_resize(vector *v, int capacity)
{
    void **items = realloc(v->items, sizeof(void *) * capacity);
    if (items)
    {
        v->items = items;
        v->capacity = capacity;
    }
}

void vector_insert(vector *v, int index, void *item)
{
    if (index < 0 || index >= vector_size(v))
        return;

    if (v->capacity == vector_size(v))
        vector_resize(v, v->capacity * 2);

    v->items[vector_size(v)] = NULL;
    for (int idx = vector_size(v); idx > index; idx--)
    {
        v->items[idx] = v->items[idx - 1];
        v->items[idx - 1] = NULL;
    }

    v->items[index] = item;
    v->total++;
}

void vector_set(vector *v, int index, void *item)
{
    if (index >= 0 && index < vector_size(v))
        v->items[index] = item;
}

void *vector_get(vector *v, int index)
{
    if (index >= 0 && index < vector_size(v))
        return v->items[index];
    return NULL;
}

void vector_push_back(vector *v, void *item)
{
    if (v->capacity == vector_size(v))
        vector_resize(v, v->capacity * 2);
    v->items[v->total++] = item;
}

void vector_pop_back(vector *v, int index)
{
    if (index < 0 || index >= vector_size(v))
        return;

    v->items[index] = NULL;
    for (int idx = index; idx < vector_size(v); idx++)
    {
        v->items[idx] = v->items[idx + 1];
        v->items[idx + 1] = NULL;
    }

    v->total--;

    if (vector_size(v) > 0 && vector_size(v) <= v->capacity / 4)
        vector_resize(v, v->capacity / 2);
}

void vector_clear(vector *v)
{
    vector_set(v, 0, NULL);
    v->capacity = 1;
    v->total = 0;
    vector_resize(v, v->capacity);
}

void vector_free(vector *v)
{
    free(v->items);
}

int vector_string_length(vector *v)
{
    int size = 0;
    for (int idx = 0; idx < vector_size(v); idx++)
        size += strlen((char *)vector_get(v, idx));
    return size;
}

void vector_stringify(vector *v, char *str, int length)
{
    for (int idx = 0; idx < vector_size(v); idx++)
    {
        if (idx == 0)
            strcpy(str, (char *)vector_get(v, idx));
        else
            strcat(str, (char *)vector_get(v, idx));
    }
    str[length - 1] = '\0'; // null-terminate
}

int vector_string_length_from_char(vector *v)
{
    return vector_size(v);
}

void vector_stringify_from_char(vector *v, char *str, int length)
{
    for (int idx = 0; idx < vector_size(v); idx++)
        str[idx] = (char)vector_get(v, idx);
    str[length - 1] = '\0'; // null-terminate
}

#if 0
int main(void)
{
    vector v;
    vector_init(&v, 1);

    vector_push_back(&v, "This");
    vector_push_back(&v, "Is");
    vector_push_back(&v, "A");
    vector_push_back(&v, "Test");
    vector_push_back(&v, "String!");

    printf("\nvector_push_back:\n");
    for (int idx = 0; idx < vector_size(&v); idx++)
        printf("%s", (char *)vector_get(&v, idx));
    printf("\n");

    char sampleStr1[vector_string_length(&v) + 1]; // extra byte for null termination
    vector_stringify(&v, sampleStr1, sizeof(sampleStr1));

    printf("\nvector_stringify:\n");
    printf("%s | %d\n", sampleStr1, vector_string_length(&v));
    printf("%c, %c, %s\n", sampleStr1[sizeof(sampleStr1) - 3], sampleStr1[sizeof(sampleStr1) - 2],
           (sampleStr1[sizeof(sampleStr1) - 1] ? sampleStr1[sizeof(sampleStr1) - 1] : "NULL"));

    printf("\nvector_pop_back:\n");
    while (vector_size(&v))
    {
        printf("%s(%d)|", (char *)vector_get(&v, 0), vector_size(&v));
        vector_pop_back(&v, 0);
        printf("%s(%d), ", (char *)vector_get(&v, 0), vector_size(&v));
    }
    printf("\n");

    vector_push_back(&v, 'a');
    vector_push_back(&v, '2');
    vector_push_back(&v, 'b');
    vector_push_back(&v, '1');
    vector_push_back(&v, 'c');
    vector_push_back(&v, '5');
    vector_push_back(&v, 'a');
    vector_push_back(&v, '3');

    printf("\nvector_push_back:\n");
    for (int idx = 0; idx < vector_size(&v); idx++)
        printf("%c", (char)vector_get(&v, idx));
    printf("\n");

    char sampleStr2[vector_string_length_from_char(&v) + 1]; // extra byte for null termination
    vector_stringify_from_char(&v, sampleStr2, sizeof(sampleStr2));

    printf("\nvector_stringify_from_char:\n");
    printf("%s | %d\n", sampleStr2, vector_string_length_from_char(&v));
    printf("%c, %c, %s\n", sampleStr2[sizeof(sampleStr2) - 3], sampleStr2[sizeof(sampleStr2) - 2],
           (sampleStr2[sizeof(sampleStr2) - 1] ? sampleStr2[sizeof(sampleStr2) - 1] : "NULL"));

    printf("\nvector_set:\n");
    for (int idx = 0; idx < vector_size(&v); idx++)
        vector_set(&v, idx, idx);

    for (int idx = 0; idx < vector_size(&v); idx++)
        printf("%d(%d), ", (int)vector_get(&v, idx), vector_size(&v));
    printf("\n");

    printf("\nvector_insert:\n");
    vector_insert(&v, 2, 10);

    for (int idx = 0; idx < vector_size(&v); idx++)
        printf("%d(%d), ", (int)vector_get(&v, idx), vector_size(&v));
    printf("\n");

    printf("\nvector_pop_back:\n");
    while (vector_size(&v))
    {
        printf("%d(%d)|", (int)vector_get(&v, 0), vector_size(&v));
        vector_pop_back(&v, 0);
        printf("%d(%d), ", (int)vector_get(&v, 0), vector_size(&v));
    }
    printf("\n");

    vector_free(&v);

    return 0;
}
#endif

#ifndef FORWARD_LIST_H
#define FORWARD_LIST_H

typedef struct node
{
    void *data;
    struct node *next;
} node;

typedef struct
{
    node *front;
    int size;
} forward_list;

void forward_list_init(forward_list *);
int forward_list_size(forward_list *);
void forward_list_insert_front(forward_list *, void *);
void forward_list_insert(forward_list *, int, void *);
void forward_list_insert_after(forward_list *, int, void *);
void *forward_list_get_front(forward_list *);
void forward_list_pop_front(forward_list *);
void forward_list_free(forward_list *);

#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "queue.h"

void queue_init(queue *q)
{
    list_init(&q->queue_list);
}

int queue_size(queue *q)
{
    return list_size(&q->queue_list);
}

void queue_push(queue *q, void *data)
{
    list_insert_rear(&q->queue_list, data);
}

void queue_pop(queue *q)
{
    list_pop_front(&q->queue_list);
}

void *queue_front(queue *q)
{
    return list_get_front(&q->queue_list);
}

void *queue_back(queue *q)
{
    return list_get_rear(&q->queue_list);
}

void queue_free(queue *q)
{
    list_free(&q->queue_list);
}

// #if 0
int main(void)
{
    queue q;
    queue_init(&q);

    queue_push(&q, 1);
    queue_push(&q, 2);
    queue_push(&q, 3);
    queue_push(&q, 4);
    queue_push(&q, 5);

    while (queue_size(&q))
    {
        printf("%d ", queue_front(&q));
        queue_pop(&q);
    }
    printf("Size: %d\n", queue_size(&q));

    queue_free(&q);

    return 0;
}
// #endif
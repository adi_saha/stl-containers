#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "list.h"

void list_init(list *d)
{
    d->front = NULL;
    d->rear = NULL;
    d->size = 0;
}

int list_size(list *d)
{
    return d->size;
}

static node *list_add_node(void *data)
{
    node *newNode = (node *)malloc(sizeof(node));
    if (newNode)
    {
        newNode->data = data;
        newNode->next = newNode->previous = NULL;
    }
    return newNode;
}

void list_insert_front(list *d, void *data)
{
    node *newNode = list_add_node(data);

    if (d->front == NULL)
        d->front = d->rear = newNode;
    else
    {
        newNode->next = d->front;
        d->front->previous = newNode;
        d->front = newNode;
    }
    d->size++;
}

void list_insert(list *d, int index, void *data)
{
    if (index < 0 || index >= list_size(d))
        return;

    node *newNode = list_add_node(data);
    node *currNode = d->front;
    for (int idx = 0; idx < list_size(d); idx++, currNode = currNode->next)
        if (idx == index)
            break;

    currNode->previous->next = newNode;
    newNode->previous = currNode->previous;
    newNode->next = currNode;
    currNode->previous = newNode;
    d->size++;
}

void list_insert_rear(list *d, void *data)
{
    node *newNode = list_add_node(data);

    if (d->rear == NULL)
        d->rear = d->front = newNode;
    else
    {
        newNode->previous = d->rear;
        d->rear->next = newNode;
        d->rear = newNode;
    }
    d->size++;
}

void *list_get_front(list *d)
{
    if (list_size(d))
        return d->front->data;
    return NULL;
}

void *list_get_rear(list *d)
{
    if (list_size(d))
        return d->rear->data;
    return NULL;
}

void list_pop_front(list *d)
{
    if (list_size(d))
    {
        node *temp = d->front;
        d->front = d->front->next;

        if (d->front == NULL)
            d->rear = NULL;
        else
            d->front->previous = NULL;

        free(temp);
        d->size--;
    }
}

void list_pop_rear(list *d)
{
    if (list_size(d))
    {
        node *temp = d->rear;
        d->rear = d->rear->previous;

        if (d->rear == NULL)
            d->front = NULL;
        else
            d->rear->next = NULL;

        free(temp);
        d->size--;
    }
}

void list_erase(list *d, int index)
{
    if (index < 0 || index >= list_size(d))
        return;

    if (index == 0)
        list_pop_front(d);
    else if (index == list_size(d) - 1)
        list_pop_rear(d);
    else
    {
        node *currNode = d->front;
        for (int idx = 0; idx < list_size(d); idx++, currNode = currNode->next)
            if (idx == index)
                break;
        currNode->previous->next = currNode->next;
        currNode->next->previous = currNode->previous;
        free(currNode);
        d->size--;
    }
}

void list_free(list *d)
{
    while (list_size(d))
        list_pop_front(d);
}

#if 0
int main(void)
{
    list l;
    list_init(&l);

    list_insert_rear(&l, "This");
    list_insert_rear(&l, "is");
    list_insert_rear(&l, "a");
    list_insert_rear(&l, "test");
    list_insert_rear(&l, "string!");

    list_insert(&l, 1, "insert-1");
    list_insert(&l, 2, "insert-2");
    list_insert(&l, 3, "insert-3");

    list_erase(&l, 0);
    list_erase(&l, 0);
    list_erase(&l, 0);

    while (list_size(&l))
    {
        printf("%s ", (char *)list_get_front(&l));
        list_pop_front(&l);
    }
    printf("\nSize: %d\n", list_size(&l));

    list_free(&l);

    return 0;
}
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "deque.h"

void deque_init(deque *d)
{
    d->size = 0;
    d->headBuffer = 1;
    d->tailBuffer = 1;
    d->startIndex = d->headBuffer;
    d->capacity = d->size + d->headBuffer + d->tailBuffer;
    d->items = malloc(sizeof(void *) * d->capacity);

    // printf("%s: head=%d, start=%d, size=%d, tail=%d, cap=%d\n", __FUNCTION__,
    //        d->headBuffer, d->startIndex, d->size, d->tailBuffer, d->capacity);
}

int deque_size(deque *d)
{
    return d->size;
}

static void deque_resize_back(deque *d, int capacity)
{
    void **items = realloc(d->items, sizeof(void *) * capacity);
    if (items)
    {
        int sizeDiff = abs(capacity - d->capacity);
        if (capacity > d->capacity)
            d->tailBuffer += sizeDiff;
        else
            d->tailBuffer -= sizeDiff;
        d->items = items;
        d->capacity = capacity;
    }
}

void deque_push_back(deque *d, void *data)
{
    if (d->tailBuffer == 0)
        deque_resize_back(d, d->capacity * 2);
    d->items[d->startIndex + deque_size(d)] = data;
    d->size++;
    d->tailBuffer--;

    // printf("%s: head=%d, start=%d, size=%d, tail=%d, cap=%d\n", __FUNCTION__,
    //        d->headBuffer, d->startIndex, d->size, d->tailBuffer, d->capacity);
}

void deque_pop_back(deque *d)
{
    d->items[d->startIndex + deque_size(d) - 1] = NULL;
    d->size--;
    d->tailBuffer++;

    if (deque_size(d) > 0 && deque_size(d) <= ((d->capacity - d->headBuffer) / 4))
        deque_resize_back(d, (d->capacity - d->headBuffer) / 2);

    // printf("%s: head=%d, start=%d, size=%d, tail=%d, cap=%d\n", __FUNCTION__,
    //        d->headBuffer, d->startIndex, d->size, d->tailBuffer, d->capacity);
}

void *deque_get_back(deque *d)
{
    if (deque_size(d))
        return d->items[d->startIndex + deque_size(d) - 1];
    return NULL;
}

static void deque_resize_front(deque *d, int capacity)
{
    void **oldPtr = d->items;
    int oldSize = deque_size(d);
    int oldStartIndex = d->startIndex;
    void **items = malloc(sizeof(void *) * capacity);
    if (items)
    {
        int sizeDiff = abs(capacity - d->capacity);
        if (capacity > d->capacity)
            d->headBuffer += sizeDiff;
        else
            d->headBuffer -= sizeDiff;

        d->items = items;
        d->startIndex = d->headBuffer;
        d->capacity = capacity;

        for (int idx = 0; idx < oldSize; idx++)
            memcpy(&d->items[d->startIndex + idx],
                   &oldPtr[oldStartIndex + idx],
                   sizeof(oldPtr[oldStartIndex + idx]));

        free(oldPtr);
    }
}

void deque_push_front(deque *d, void *data)
{
    if (d->headBuffer == 0)
        deque_resize_front(d, d->capacity * 2);
    d->items[--d->startIndex] = data;
    d->headBuffer--;
    d->size++;

    // printf("%s: head=%d, start=%d, size=%d, tail=%d, cap=%d\n", __FUNCTION__,
    //        d->headBuffer, d->startIndex, d->size, d->tailBuffer, d->capacity);
}

void deque_pop_front(deque *d)
{
    d->items[d->startIndex] = NULL;
    d->headBuffer++;
    d->startIndex = d->headBuffer;
    d->size--;

    if (deque_size(d) > 0 && deque_size(d) <= ((d->capacity - d->tailBuffer) / 4))
        deque_resize_front(d, (d->capacity - d->tailBuffer) / 2);

    // printf("%s: head=%d, start=%d, size=%d, tail=%d, cap=%d\n", __FUNCTION__,
    //        d->headBuffer, d->startIndex, d->size, d->tailBuffer, d->capacity);
}

void *deque_get_front(deque *d)
{
    if (deque_size(d))
        return d->items[d->startIndex];
    return NULL;
}

// void deque_insert(deque *d, int index, void *data)
// {
// }

// void deque_erase(deque *d, int index)
// {
// }

void deque_free(deque *d)
{
    free(d->items);
}

// #if 0
int main(void)
{
    deque d;
    deque_init(&d);

    deque_push_front(&d, "This");
    deque_push_front(&d, "is");
    deque_push_front(&d, "a");
    deque_push_front(&d, "test");
    deque_push_front(&d, "string!");

    while (deque_size(&d))
    {
        printf("%s ", (char *)deque_get_back(&d));
        deque_pop_back(&d);
    }
    printf("Size: %d\n", deque_size(&d));

    deque_free(&d);

    return 0;
}
// endif
